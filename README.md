## Registro Momentum
### vistas
En las vistas  se accede al index mediante la ruta

http://localhost:8080/registroMomentum/

aqui podremos encontrar un menu con las diferentes acciones que podemos llevar a cabo.
### Inicializacion del codigo

Para poder inicializar el codigo es necesario entrar a la ruta:

http://localhost:8080/registroMomentum/api/v1/upload

es un metodo post en donde se pedira un archivo xls
- file

La base de datos se llama sesion3
que contiene 2 tablas la de usuarios y la de momentum

### Endpoints

#### api/v1/users/IS/mouth

method get

este enpoin se debe de mandar por la url el is y el mes en dos numeros (01) que se quiere revisar para que de las horas del usuario

http://localhost:8080/registroMomentum/api/v1/users/IS/mouth

#### api/v1/users
method post

http://localhost:8080/registroMomentum/api/v1/users

este es un metodo post en el cual el body debera de contener:
 - is
 - startDate
 - endDate

#### api/v1/period/04

method get

http://localhost:8080/registroMomentum/api/v1/period/mounth

este es un metodo get por lo que el mes que se desea ver se debera de poner en la url con dos digitos (01)

#### api/v1/period

method post

http://localhost:8080/registroMomentum/api/v1/period

este es un metodo post el cual debe de llevar como body 
 - startDate
 - endDate

