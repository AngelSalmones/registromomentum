package com.softtek.pfinal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.softtek.pfinal.model.Momentum;
import com.softtek.pfinal.model.Users;

@SpringBootTest
public class RegistrosMomentumImplTest {

	@Test
	public void getusersTest() {
		RegistroMomentumImpl reg =  new RegistroMomentumImpl();
		List<Users> users = reg.getUsers();
		String nameExpected = "CXMG";
		String nameActual = users.get(0).getName();
		assertNotNull(users);
		assertEquals(nameExpected, nameActual);
	}
	@Test
	public void getRegistrosTest () {
		RegistroMomentumImpl reg =  new RegistroMomentumImpl();
		List<Momentum> momentums  = reg.getRegistros();
		String nameExpected = "CASV1";
		String nameActual = momentums.get(0).getName();
		assertNotNull(momentums);
		assertEquals(nameExpected, nameActual);
	}
	
	@Test
	public void filterRegisterTest () {
		RegistroMomentumImpl reg =  new RegistroMomentumImpl();
		List<Momentum> momentums  = reg.filterRegister();
		String nameExpected = "AAAC1";
		String nameActual = momentums.get(0).getName();
		assertNotNull(momentums);
		assertEquals(nameExpected, nameActual);
	}
	
}
