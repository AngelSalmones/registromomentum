package com.softtek.pfinal.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.softtek.pfinal.model.Momentum;

@SpringBootTest
public class MomentumServiceImplTest {
	@Autowired
	MomentumService momentumService;
	

	@Test
	public void getByMounthTest () {
	List<Momentum> registrosMes = momentumService.getByMounth("02");
	String nameExpected = "ADEL";
	String nameActual = registrosMes.get(0).getName();
	assertNotNull(registrosMes);
	assertEquals(nameExpected, nameActual);
	}
}
