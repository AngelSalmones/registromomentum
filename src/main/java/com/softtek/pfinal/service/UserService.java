package com.softtek.pfinal.service;

import java.util.List;

import com.softtek.pfinal.model.Users;

public interface UserService {
	Users createUser (List<Users> users);
	Users getUser (String IS);
	List<Users> getAllUsers();
	void create(Users users);
}
