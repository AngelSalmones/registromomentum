package com.softtek.pfinal.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.softtek.pfinal.model.Users;
import com.softtek.pfinal.repository.UserRepository;

@Service("UserService")
public class UserServiceImpl implements UserService{

	@Autowired
	@Qualifier("UserRepository")
	private UserRepository userRepository;

	@Override
	public Users createUser(List<Users> users) {
		for(int i =0; i<users.size();i++) {
			Users user = new Users();
			user = users.get(i);
			userRepository.save(user);
		}
		return users.get(0);
	}

	@Override
	public Users getUser(String IS) {
		List<Users> users = userRepository.findAll();
		Users user = new Users();
		for(int i =0; i< users.size();i++) {
			if(users.get(i).getName().equals(IS)) {
				user = users.get(i);
			}
		}
		return user;
	}

	@Override
	public List<Users> getAllUsers() {
		
		return userRepository.findAll();
	}

	@Override
	public void create(Users users) {
		userRepository.save(users);
		
	}

	

}
