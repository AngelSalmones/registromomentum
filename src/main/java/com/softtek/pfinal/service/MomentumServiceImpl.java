package com.softtek.pfinal.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.softtek.pfinal.model.Horas;
import com.softtek.pfinal.model.Momentum;
import com.softtek.pfinal.repository.MomentumRepository;

@Service("MomentumService")
public class MomentumServiceImpl implements MomentumService{

	@Autowired
	@Qualifier("MomentumRepository")
	private MomentumRepository momentumRepository;
	
	@Override
	public Momentum createData(List<Momentum> registros) {
		registros.forEach(c->{
			momentumRepository.save(c);
		});
		return null;
	}

	@Override
	public Momentum createone(Momentum momentun) {
		return momentumRepository.save(momentun);
	}

	@Override
	public List<Momentum> getAllRegistro() {
		return momentumRepository.findAll();
	}

	@Override
	public Horas getHoursByISandMount(String is, String mes) {
		List<Momentum> data = momentumRepository.findByName(is);
		List<LocalTime> time = new ArrayList<>();
		List<LocalDate> fecha = new ArrayList<>();
		Horas horas = new Horas ();
		String [] date ;
		String []  mouth;
		long comparing = 0;
		long hour1 = 0;
		long min1 = 0;
		long sec1 = 0;
		long hour2 = 0;
		long min2 = 0;
		long sec2 = 0;
		long ht = 0;
		long mt = 0;
		long st = 0;
		long res = 0;
		List<Momentum> datafilter = new ArrayList<>();
		for(int i = 0;i<data.size();i++) {
			date = data.get(i).getDatetime().split(" ");
			mouth = date[0].split("-");
			if(mouth[1].equals(mes)) {
				datafilter.add(data.get(i));
				time.add(LocalTime.parse(date[1]));
				fecha.add(LocalDate.parse(date[0]));
			}
		}
		
		for(int i = 1 ;i<fecha.size();i++) {
			if(fecha.get(i-1).equals(fecha.get(i))) {
				if(time.get(i-1).getHour()<time.get(i).getHour()) {
					 hour1 = time.get(i-1).getHour();
					 min1 = time.get(i-1).getMinute();
					 sec1 = time.get(i-1).getSecond();
					 hour2 = time.get(i).getHour();
					 min2 = time.get(i).getMinute();
					 sec2 = time.get(i).getSecond();
					 
					 hour1 = hour1 * 3600;
					 min1 = min1 * 60;
					 hour2 = hour2 * 3600;
					 min2 = min2 * 60;
					 sec1 = sec1 + min1 + hour1;
					 sec2 = sec2 + min2 + hour2;
					 comparing = comparing + (sec2 - sec1);
				}
			}
		}
		
		
		if(comparing==0) {
			
		}else {
			res = comparing%3600;
			ht = comparing / 3600;
			mt = res/60;
			st = res%60;
			horas.setHoras(ht);
			horas.setMinutos(mt);
			horas.setSegundos(st);
		}
		return   horas ;
	}

	@Override
	public List<Momentum> getByMounth(String mes) {
		List<Momentum> data = momentumRepository.findAll();
		List<Momentum> result = new ArrayList<>();
		String [] date;
		String [] mouth;
		for(int i = 0;i<data.size();i++) {
			date = data.get(i).getDatetime().split(" ");
			mouth = date[0].split("-");
			System.out.println(mouth[1]);
			if(mouth[1].equals(mes)) {
				result.add(data.get(i));
			}
		}
		return result;
		
	}

	@Override
	public List<Momentum> getHoursByIs(String is, String startDate, String endDate) {
		List<Momentum> data = momentumRepository.findByName(is);
		List<Momentum> datafilter = new ArrayList<>();
		
		String [] date ;
		LocalDate fecha1;
		LocalDate fechaStart = LocalDate.parse(startDate);
		LocalDate fechaEnd = LocalDate.parse(endDate);
		
		for(int i = 0;i<data.size();i++) {
			date = data.get(i).getDatetime().split(" ");
			fecha1 = LocalDate.parse(date[0]);
			if((date[0].equals(startDate))||(date[0].equals(endDate))) {
				datafilter.add(data.get(i));
			}else if(fecha1.isAfter(fechaStart)) {
				if(fecha1.isBefore(fechaEnd)) {
					datafilter.add(data.get(i));
				}
			}
		}

		return datafilter;
	}

	@Override
	public List<Momentum> getRegistrosByStartEndDay(String startDate, String endDate) {
		List<Momentum> data = momentumRepository.findAll();
		List<Momentum> result = new ArrayList<>();
		String [] date ;
		LocalDate fecha1;
		LocalDate fechaStart = LocalDate.parse(startDate);
		LocalDate fechaEnd = LocalDate.parse(endDate);
		
		for(int i = 0;i<data.size();i++) {
			date = data.get(i).getDatetime().split(" ");
			fecha1 = LocalDate.parse(date[0]);
			if((date[0].equals(startDate))||(date[0].equals(endDate))) {
				result.add(data.get(i));
			}else if(fecha1.isAfter(fechaStart)) {
				if(fecha1.isBefore(fechaEnd)) {
					result.add(data.get(i));
				}
			}
		}
		return result;
	}

}
