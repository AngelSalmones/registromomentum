package com.softtek.pfinal.service;

import java.util.List;

import com.softtek.pfinal.model.Horas;
import com.softtek.pfinal.model.Momentum;

public interface MomentumService {
	Momentum createData (List<Momentum> registros);
	Momentum createone (Momentum momentun);
	List<Momentum> getAllRegistro ();
	Horas getHoursByISandMount(String is, String mes);
	List<Momentum> getByMounth(String mes);
	List<Momentum> getHoursByIs(String is, String startDate, String endDate);
	List<Momentum> getRegistrosByStartEndDay(String startDate, String endDate);
}
