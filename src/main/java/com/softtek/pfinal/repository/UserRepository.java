package com.softtek.pfinal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.softtek.pfinal.model.Users;

@Repository("UserRepository")
public interface UserRepository extends JpaRepository<Users, Integer>{
}
