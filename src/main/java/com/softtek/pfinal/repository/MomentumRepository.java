package com.softtek.pfinal.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.softtek.pfinal.model.Momentum;
@Repository("MomentumRepository")
public interface MomentumRepository extends JpaRepository<Momentum, Integer> {

	List<Momentum> findByName(String is);

	List<Momentum> findByNameAndDatetime(String is, String string);


}
