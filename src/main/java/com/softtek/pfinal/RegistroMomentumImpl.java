package com.softtek.pfinal;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.softtek.pfinal.model.Momentum;
import com.softtek.pfinal.model.Users;


public class RegistroMomentumImpl {
	String rutaArchivo = "C:\\Users\\angele.garcia\\Documents\\workspace-spring-tool-suite-4-4.4.1.RELEASE\\RegistroMomentum\\src\\main\\resources\\utils\\RegistrosMomentum.xls";
	int cont = 0;
	int id = 1;
	String cellnum;
	String celldep;
	String cellid;
	String cellverifi;
	String celldeviceid;
	String celldevicename;
	String cellname;
	String celldate;
	String cellinout;
	
	List<String> names = new ArrayList<>();
	List<Users> users = new ArrayList<>();
	List<Momentum> registros = new ArrayList<>();
	List<Momentum> registrosF = new ArrayList<>();
	
	public void getFile () {
	try (FileInputStream file = new FileInputStream(new File(rutaArchivo))) {
		HSSFWorkbook worbook = new HSSFWorkbook(file);
		HSSFSheet sheet = worbook.getSheetAt(0);
		Iterator<Row> rowIterator = sheet.iterator();

		Row row;
		while (rowIterator.hasNext()) {
			cont++;
			row = rowIterator.next();
			Iterator<Cell> cellIterator = row.cellIterator();
			Cell cell;
			if(cont>1) {
			while (cellIterator.hasNext()) {
				cell = cellIterator.next();
				if(cell.getColumnIndex()==0){
					cellnum = cell.getStringCellValue();
				}
				if(cell.getColumnIndex()==1) {
					celldep = cell.getStringCellValue();
				}
				if (cell.getColumnIndex()==2) {
					cellname = cell.getStringCellValue();
					names.add(cellname);
				}
				if(cell.getColumnIndex()==3) {
					cellid = cell.getStringCellValue();
				}
				if (cell.getColumnIndex()==4) {
				    celldate = cell.getStringCellValue();
				}
				if(cell.getColumnIndex()==5) {
					cellverifi = cell.getStringCellValue();
				}
				if (cell.getColumnIndex()==6) {
				    cellinout = cell.getStringCellValue();
				}
				if(cell.getColumnIndex()==7) {
					celldeviceid = cell.getStringCellValue();
				}
				if(cell.getColumnIndex()==8) {
					celldevicename = cell.getStringCellValue();
				}
				
			}
			Momentum registro = new Momentum();
			registro.setDepartment(celldep);
			registro.setDeviceid(celldeviceid);
			registro.setDevicename(celldevicename);
			registro.setNum(cellnum);
			registro.setVerifycode(cellverifi);
			registro.setId(cellid);
			registro.setName(cellname);
			registro.setDatetime(celldate);
			registro.setClockinout(cellinout);
			registros.add(registro);
			id++;
			}
		 
		}
		
		
		String temp = names.get(0);
		for (int i = 0 ; i<names.size();i++) {
			if(temp.equals(names.get(i))) {
				
			}else {
				Users user = new Users();
				user.setName(names.get(i));
				users.add(user);
			}
			temp = names.get(i);
		}
	}	 catch (Exception e) {
		System.out.println(e);
		e.getMessage();
	}
  }
	
	public List<Users> getUsers (){
		getFile();
		return users;
	}
	
	public List<Momentum> getRegistros (){
		getFile();
		return registros;
	}
	
	public List<Momentum> filterRegister () {
		registros.clear();
		registrosF.clear();
		getFile();
			registros.sort(Comparator.comparing(Momentum::getName)
					.thenComparing(Momentum::getDatetime)
					.thenComparing(Momentum::getClockinout));
			String name1 = registros.get(0).getName();
			String name2;
			String [] date1 = null;
			String [] date2 = null;
		
			name1 = registros.get(0).getName();
			date1 = registros.get(0).getDatetime().split(" ");
			
			for(int i = 0;i<registros.size()-1;i++){
				name1 = registros.get(i).getName();
				name2 = registros.get(i+1).getName();
				if(name1.equals(name2)) {
					date1 = registros.get(i).getDatetime().split(" ");
					date2 = registros.get(i+1).getDatetime().split(" ");
					if(date1[0].equals(date2[0])) {
						registrosF.add(registros.get(i));
						registrosF.add(registros.get(i+1));
					}
				}
			}
			
		
			System.out.println(registrosF.size());
			System.out.println(users.size());
			
			registrosF.sort(Comparator.comparing(Momentum::getName)
					.thenComparing(Momentum::getDatetime)
					.thenComparing(Momentum::getClockinout));
			
			
			return registrosF;
	}
}

