package com.softtek.pfinal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
//bean the registers momentum
@Entity
public class Momentum {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "registerid")
	@NotNull
	private int registerId;
	@NotBlank
	private String num;
	@NotBlank
	private String department;
	@NotBlank
	private String id;
	@NotBlank
	private String verifycode;
	@NotBlank
	private String deviceid;
	@NotBlank
	private String devicename;
	@NotBlank
	private String name;
	@NotBlank
	private String datetime;
	@NotBlank
	private String clockinout;
	
	public Momentum() {
		
	}

	public int getRegisterId() {
		return registerId;
	}

	public void setRegisterId(int registerId) {
		this.registerId = registerId;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getVerifycode() {
		return verifycode;
	}

	public void setVerifycode(String verifycode) {
		this.verifycode = verifycode;
	}

	public String getDeviceid() {
		return deviceid;
	}

	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}

	public String getDevicename() {
		return devicename;
	}

	public void setDevicename(String devicename) {
		this.devicename = devicename;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public String getClockinout() {
		return clockinout;
	}

	public void setClockinout(String clockinout) {
		this.clockinout = clockinout;
	}

	

	
	
}
