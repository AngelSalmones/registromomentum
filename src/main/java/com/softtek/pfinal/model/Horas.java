package com.softtek.pfinal.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
//bean the hours 
@Entity
public class Horas {
	@Id
	@JsonProperty("horas")
	@NotNull
	private long horas;
	@JsonProperty("minutos")
	@NotNull
	private long minutos;
	@JsonProperty("segundos")
	@NotNull
	private long segundos;
	
	public Horas() {}

	public long getHoras() {
		return horas;
	}

	public void setHoras(long horas) {
		this.horas = horas;
	}

	public long getMinutos() {
		return minutos;
	}

	public void setMinutos(long minutos) {
		this.minutos = minutos;
	}

	public long getSegundos() {
		return segundos;
	}

	public void setSegundos(long segundos) {
		this.segundos = segundos;
	}

	@Override
	public String toString() {
		return "Horas [horas=" + horas + ", minutos=" + minutos + ", segundos=" + segundos + "]";
	}

	
	
}
