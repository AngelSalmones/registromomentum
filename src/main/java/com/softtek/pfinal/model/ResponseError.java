package com.softtek.pfinal.model;
//model the response error

public class ResponseError {
	
	private String timestamp;
	private int statusCode;
	private String error;
	private String message;
	private String path;

	public ResponseError() {}
	
	public ResponseError(String timestamp, int statusCode, String error, String message, String path) {
		this.timestamp = timestamp;
		this.statusCode = statusCode;
		this.error = error;
		this.message = message;
		this.path = path;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	@Override
	public String toString() {
		return "timestamp=" + timestamp + "\n statusCode=" + statusCode + "\n error=" + error
				+ "\n message=" + message + "\n path=" + path;
	}
	

}
