package com.softtek.pfinal.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.softtek.pfinal.RegistroMomentumImpl;
import com.softtek.pfinal.model.Horas;
import com.softtek.pfinal.model.Momentum;
import com.softtek.pfinal.model.ResponseError;
import com.softtek.pfinal.model.Users;
import com.softtek.pfinal.service.MomentumService;
import com.softtek.pfinal.service.UserService;
//controller init
@Controller
public class InitController {
	private final Logger logger = LoggerFactory.getLogger(ApiJsonController.class);
	private static String UPLOADED_FOLDER = "C:\\Users\\angele.garcia\\Documents\\workspace-spring-tool-suite-4-4.4.1.RELEASE\\RegistroMomentum\\src\\main\\resources\\utils\\";
	
	@Autowired
	UserService userService;
	@Autowired
	MomentumService momentumService;
	
	RegistroMomentumImpl reg = new RegistroMomentumImpl();
	
	private void saveUploadedFiles(List<MultipartFile> files) throws IOException {
		
        for (MultipartFile file : files) {
            if (file.isEmpty()) {
                continue;
            }
            byte[] bytes = file.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + "RegistrosMomentum.xls");
            Files.write(path, bytes);
        }

    }

	@PostMapping("api/v1/upload")
    public ResponseEntity<?> uploadFile(
            @RequestParam("file") MultipartFile uploadfile) {
	 	logger.debug("Single file upload!");
        if (uploadfile.isEmpty()) {
            return new ResponseEntity("please select a file!", HttpStatus.OK);
        }
        try {
            saveUploadedFiles(Arrays.asList(uploadfile));

        } catch (IOException e) {
        	System.out.println(e.toString());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<Momentum> list = new ArrayList<>();
        list = getFile();
        return new ResponseEntity(list
               , new HttpHeaders(), HttpStatus.OK);
    }
	 @PostMapping("/uploadview")
	    public String uploadFileview(
	            @RequestParam("file") MultipartFile uploadfile, Model model) {
		 logger.debug("Single file upload!");
	        if (uploadfile.isEmpty()) {
	        	model.addAttribute("AllList","please select a file!");
	        	return "allRegistros";
	        }
	        try {
	            saveUploadedFiles(Arrays.asList(uploadfile));

	        } catch (IOException e) {
	        	System.out.println(e.toString());
	        	model.addAttribute("AllList","please select a file!");
	        	
	        	return "erroruploadFile";
	        }
	        List<Momentum> list = new ArrayList<>();
	        list = getFile();
	        model.addAttribute("AllList", list);
	        return "allRegistros";
	    }
	
	 @RequestMapping(value = "/getuser", method = RequestMethod.GET)
		public String getUserISMes( 
				@RequestParam("IS") String IS,
	            @RequestParam("mount") String mes,
	            Model model) {
			Horas result = momentumService.getHoursByISandMount(IS, mes);
			Users user = userService.getUser(IS);
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			if((result.getHoras()==0)&&(result.getMinutos()==0)&&(result.getSegundos()==0)) {
					ResponseError error = new ResponseError(timestamp.toString(),404,"Datos no validos","Los datos o fecha introducidos no son validos o el usuairo no tiene horas registradas"
							,"/registroMomentum/api/v1/users/"+ IS+"/"+mes+"");
					model.addAttribute("error",error);
					return "error";
			}else {
				model.addAttribute("hours", result);
				return "hours";
			}
				
		}
	 
	 @RequestMapping(value = "/usersregistros", method = RequestMethod.POST)
		public String gethourusers (String is, String startDate , String endDate, Model model) {
			List<Momentum> result  = momentumService.getHoursByIs(is,startDate,endDate);
			if(result.isEmpty()) {
				 Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				 ResponseError error = new ResponseError(timestamp.toString(),400,"Datos no validos","Los datos introducidos no son validos"
						,"/registroMomentum/api/v1/users");
				 model.addAttribute("error",error);
					return "error";
			}
			else {
				model.addAttribute("registros", result);
				return "registros";
			}	
			
			
		}
	 
	 @RequestMapping(value = "/periodmes", method = RequestMethod.GET)
		public String getPeriodMes (@RequestParam("mes") String mes, Model model) {
			List<Momentum> list = new ArrayList<>();
			list = momentumService.getByMounth(mes);
			if(list.isEmpty()) {
				 Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				 ResponseError error = new ResponseError(timestamp.toString(),400,"Datos no validos","Los datos introducidos no son validos"
						,"/registroMomentum/api/v1/users");
				 model.addAttribute("error",error);
					return "error";
			}
			else {
				model.addAttribute("list",list);
				return "getperiod";
			}
			
		}
		@RequestMapping(value = "/period", method = RequestMethod.POST)
		public String getPeriod(String startDate , String endDate, Model model){
			List<Momentum> list = new ArrayList<>();
			list = momentumService.getRegistrosByStartEndDay(startDate,endDate);
			if(list.isEmpty()) {
				 Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				 ResponseError error = new ResponseError(timestamp.toString(),400,"Datos no validos","Los datos introducidos no son validos"
						,"/registroMomentum/api/v1/users");
				 model.addAttribute("error",error);
					return "error";
			}
			else {
				model.addAttribute("list",list);
				return "postperiod";
			}
			
		}
	 @RequestMapping("/")
		public String index() {
			   return "index";
			}
	 
	 @RequestMapping("/up")
		public String view() {
			   return "uploadform";
			}
	 
	 @RequestMapping("/horas")
		public String fromhoras() {
			   return "formhoras";
			}
	 @RequestMapping("/users")
		public String formusers() {
			   return "formregistros";
			}
	 @RequestMapping("/postperiod")
		public String postperiod() {
			   return "formpostperiod";
			}
	 @RequestMapping("/getperiod")
		public String getperiod() {
			   return "formgetperiod";
			}
	 
	 
	//start init with the validate of lists
	@GetMapping("/init")
	public List<Momentum> getFile () {
		checkdataUsers();
		checkRegistros();
		return momentumService.getAllRegistro();
	}

	//insert users into database 
	@GetMapping("/getusers")
	public String getUsers() {
		List<Users> users = new ArrayList<>();
		users = reg.getUsers();
		userService.createUser(users);
		return "";
	}
	//insert registros into database
	@GetMapping("/registrosfiltrados")
	public List<Momentum> getRegistrosfiltrados (){
		List<Momentum> registros = new ArrayList<>();
		registros = reg.filterRegister();
		//momentumService.createone(registros.get(0));
		momentumService.createData(registros);
		return registros; 
	}
	//valid the database is empty
	@RequestMapping(value = "/checkdataregistros", method = RequestMethod.GET)
	public void checkRegistros () {
		List<Momentum> listmomentumdb = momentumService.getAllRegistro();
		if(listmomentumdb.isEmpty()) {
			getRegistrosfiltrados();
		}
		else {
			validatorRegistro();
		}
	}
	//check the database in the table users if empty insert the new users
	@GetMapping("/checkdatausers")
	public void checkdataUsers () {
		List<Users> listUsersdb = userService.getAllUsers();
		if(listUsersdb.isEmpty()) {
			getUsers();
		}
		else {
			validatoruser();
		}
	}
	//valid the users from a new xsl and the users from database
	@GetMapping("/validatoruser")
	public int validatoruser () {
		List<Users> listUsersdb = userService.getAllUsers();
		List<Users> listUsersxls= reg.getUsers();
		int contador = 0;
		boolean add = true;
		for(int i = 0; i<listUsersxls.size();i++) {
			for(int j = 0;j<listUsersdb.size();j++) {
				if(listUsersxls.get(i).getName().equals(listUsersdb.get(j).getName())) {
					add = false;
				}
			}
			if(add) {
				userService.create(listUsersxls.get(i));
			}
			add = true;
		}
		System.out.println(contador);
		return contador; 
	}
	//valid the registors and the database
	@GetMapping("/validatorregistro")
	public void validatorRegistro () {
		List<Momentum> listmomentumdb = momentumService.getAllRegistro();
		List<Momentum> listmomentumxls = reg.filterRegister();
		boolean add = true;
		for(int i = 0 ; i<listmomentumxls.size();i++) {
			for (int j=0;j<listmomentumdb.size();j++) {
				if((listmomentumxls.get(i).getName().equals(listmomentumdb.get(j).getName()))&&
						(listmomentumxls.get(i).getDatetime().equals(listmomentumdb.get(j).getDatetime()))) {
					add = false;
				}
			}
			if(add) {
				momentumService.createone(listmomentumxls.get(i));
			}
			add =true;
		}
	}
	

	
}
