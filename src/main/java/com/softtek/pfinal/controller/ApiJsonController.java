package com.softtek.pfinal.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.softtek.pfinal.model.Horas;
import com.softtek.pfinal.model.Momentum;
import com.softtek.pfinal.model.ResponseError;
import com.softtek.pfinal.model.Users;
import com.softtek.pfinal.service.MomentumService;
import com.softtek.pfinal.service.UserService;
//controller api
@RestController
@RequestMapping("api/v1")
public class ApiJsonController {
	
	@Autowired
	private MomentumService momentumService;
	@Autowired
	private UserService userService;

	 
	
	@RequestMapping(value = "/users/{IS}/{mes}", method = RequestMethod.GET)
	public ResponseEntity <?> getUserISMes(@Valid @PathVariable String IS, @PathVariable String mes) {
		Horas result = momentumService.getHoursByISandMount(IS, mes);
		Users user = userService.getUser(IS);
		Integer mesnum = Integer.parseInt(mes);
		System.out.println(mesnum);
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		if(user.getName().isEmpty()) {
			return new ResponseEntity<ResponseError>(new ResponseError(timestamp.toString(),400,"Datos no validos","Los datos o fecha introducidos no son validos o el usuairo no tiene horas registradas"
					,"/registroMomentum/api/v1/users/"+IS+"/"+mes+"is")  , HttpStatus.BAD_REQUEST);
		}
		else if((result.getHoras()==0)&&(result.getMinutos()==0)&&(result.getSegundos()==0)) {
				return new ResponseEntity<ResponseError>(new ResponseError(timestamp.toString(),404,"Datos no validos","Los datos o fecha introducidos no son validos o el usuairo no tiene horas registradas"
						,"/registroMomentum/api/v1/users/"+ IS+"/"+mes+"")  , HttpStatus.NOT_FOUND);
		}else {
			return new ResponseEntity <Horas>(result,HttpStatus.OK);
		}
		
	}
	
	@RequestMapping(value = "/users", method = RequestMethod.POST)
	public ResponseEntity <?> gethourusers (String is, String startDate , String endDate) {
		List<Momentum> result  = momentumService.getHoursByIs(is,startDate,endDate);
		if(result.isEmpty()) {
			 Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			return new ResponseEntity<ResponseError>(new ResponseError(timestamp.toString(),400,"Datos no validos","Los datos introducidos no son validos"
					,"/registroMomentum/api/v1/users"), HttpStatus.BAD_REQUEST);
		}
		else {
			return new ResponseEntity<List<Momentum>>(result,HttpStatus.OK);
		}
	}
	
	
	@RequestMapping(value = "/period/{mes}", method = RequestMethod.GET)
	public ResponseEntity<?> getPeriodMes (@PathVariable String mes) {
		List<Momentum> list = new ArrayList<>();
		list = momentumService.getByMounth(mes);
		if(list.isEmpty()) {
			 Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			return new ResponseEntity<ResponseError>(new ResponseError(timestamp.toString(),400,"Datos no validos","Los datos introducidos no son validos"
					,"/registroMomentum/api/v1/users"), HttpStatus.BAD_REQUEST);
		}
		else {
			return new ResponseEntity<List<Momentum>>(list,HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/period", method = RequestMethod.POST)
	public ResponseEntity<?> getPeriod(String startDate , String endDate){
		List<Momentum> list = new ArrayList<>();
		list = momentumService.getRegistrosByStartEndDay(startDate,endDate);
		if(list.isEmpty()) {
			 Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			return new ResponseEntity<ResponseError>(new ResponseError(timestamp.toString(),400,"Datos no validos","Los datos introducidos no son validos"
					,"/registroMomentum/api/v1/users"), HttpStatus.BAD_REQUEST);
		}
		else {
			return new ResponseEntity<List<Momentum>>(list,HttpStatus.OK);
		}
	}
	
}
