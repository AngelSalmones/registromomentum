<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>

<table border="2" >
         <tr>
         <td>ID</td>
         <td>Name</td>
         <td>Date/Time</td>
         <td>Clock-in/out</td>
         </tr>
            <c:forEach var="reg" items="${registros}">
                <tr>
                        <td>${reg.id}</td>
             
                        <td>${reg.name}</td>
             
                        <td>${reg.datetime}</td>
                    
                        <td>${reg.clockinout}</td>
                 
                 </tr>
            </c:forEach>
        </table>
</head>
<body>

</body>
</html>