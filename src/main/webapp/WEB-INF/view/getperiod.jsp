<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<table border="2" >
         <tr>
         <td>ID</td>
         <td>NAME</td>
         <td>Date/Time</td>
         <td>Clock-in/out</td>
         </tr>
            <c:forEach var="reg" items="${list}">
                <tr>
                        <td>${reg.id}</td>
                    
                        <td>${reg.name}</td>
                    
                        <td>${reg.datetime}</td>
                   
                        <td>${reg.clockinout}</td>
                    
    
                 </tr>
            </c:forEach>
        </table>
</body>
</html>